﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp.StocksBot
{
    public class StocksCheck
    {
        private string _baseUrl { get; set; }
        private string _queueName { get; set; }
        private ConnectionFactory _connectionFactory { get; set; }

        public StocksCheck(string baseUrl, string queueName, ConnectionFactory connectionFactory)
        {
            _baseUrl = baseUrl;
            _queueName = queueName;
            _connectionFactory = connectionFactory;
        }

        public async Task<bool> FindStockPrice(string stockCode)
        {
            try
            {
                string message = await GetBotMessage(_baseUrl, stockCode);

                using (var connection = _connectionFactory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {
                        channel.QueueDeclare(queue: _queueName,
                                             durable: false,
                                             exclusive: false,
                                             autoDelete: false,
                                             arguments: null);

                        var body = Encoding.UTF8.GetBytes(message);

                        channel.BasicPublish(exchange: "",
                                             routingKey: _queueName,
                                             basicProperties: null,
                                             body: body);

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        private async Task<string> GetBotMessage(string baseURl, string stockCode)
        {
            try
            {
                string completeUrl = baseURl.Replace("{%STOCKCODE%}", stockCode);

                using (HttpClient httpClient = new HttpClient())
                {
                    var response = await httpClient.GetAsync(completeUrl);
                    var responseStr = await response.Content.ReadAsStringAsync();
                    string[] vs = responseStr.Split("\r\n", StringSplitOptions.RemoveEmptyEntries);

                    if (vs == null || vs.Length < 2)
                        return "Could not communicate with stocks api. Please, try again later";

                    string[] fieldsNames = vs[0].Split(",", StringSplitOptions.RemoveEmptyEntries);
                    string[] values = vs[1].Split(',', StringSplitOptions.RemoveEmptyEntries);

                    if (fieldsNames.Length != values.Length)
                        return "The stocks API returned an unexpected result. Please, try again later";

                    string stockVal = "";
                    for (int i = 0; i < fieldsNames.Length; i += 1)
                    {
                        if (fieldsNames[i].Equals("Close", StringComparison.InvariantCultureIgnoreCase))
                        {
                            stockVal = values[i];
                            break;
                        }
                    }

                    string stockValue = values[6];

                    if (decimal.TryParse(stockValue, out decimal stock))
                    {
                        return $"{stockCode.ToUpper()} quote is ${stockValue} per share.";
                    }

                    return $"Value for stock code {stockCode} could not be found";
                }
            }
            catch (Exception ex)
            {
                return "Something went wrong. Unable to get stock price.";
            }
        }
    }
}
