﻿using ChatApp.Web.Infrastructure;
using Microsoft.AspNetCore.SignalR;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

namespace ChatApp.Web.Services
{
    public class ProcessStockMessagesService : BackgroundService
    {
        private readonly IConfiguration _config;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly IHubContext<ChatHub> _hubContext;
        private readonly string _queueName;

        public ProcessStockMessagesService(IHubContext<ChatHub> hubContext, IConfiguration config)
        {
            _hubContext = hubContext;
            _config = config;

            _queueName = _config.GetSection("QueueName").Value;

            var factory = _config.GetSection("RabbitMQConnectionFactorySettings").Get<ConnectionFactory>();

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: _queueName,
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += (sender, args) => {
                var arrayContent = args.Body.ToArray();
                string strContent = Encoding.UTF8.GetString(arrayContent);
                _channel.BasicAck(args.DeliveryTag, false);
                _hubContext.Clients.All.SendAsync("Send", "StocksBot", strContent, DateTime.Now.ToString("dd/MM/yy HH:mm:ss"));
            };

            _channel.BasicConsume(_queueName, false , consumer);

            return Task.CompletedTask;

        }
    }
}
