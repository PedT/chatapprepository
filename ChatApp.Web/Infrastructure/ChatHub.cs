﻿using ChatApp.StocksBot;
using Microsoft.AspNetCore.SignalR;
using RabbitMQ.Client;

namespace ChatApp.Web.Infrastructure
{
    public class ChatHub : Hub
    {
        
        private IConfiguration _configuration;
        private StocksCheck _stocksCheck;

        public ChatHub(IConfiguration configuration)
        {
            _configuration = configuration;

            string baseUrl = _configuration.GetSection("StocksApiUrl").Value;
            string queueName = _configuration.GetSection("QueueName").Value;
            var factory = _configuration.GetSection("RabbitMQConnectionFactorySettings").Get<ConnectionFactory>();
            _stocksCheck = new StocksCheck(baseUrl, queueName, factory);
        }

        public async Task Send(string userName, string message)
        {
            if (message != null && message.StartsWith("/stock="))
            {
                string stockCode = message.Substring(7);
                _stocksCheck.FindStockPrice(stockCode);
            }
            else
            {
                await Clients.All.SendAsync("Send", userName, message, DateTime.Now.ToString("dd/MM/yy HH:mm:ss"));
            }
        }
    }
}
