﻿var connection = null;

$(document).ready(function () {

    $('#NewMessage').on('keypress', e => {
        if (e.keyCode == 13) {
            SendMessage();
        }
    })


    connection = new signalR.HubConnectionBuilder()
        .withUrl("/chat")
        .configureLogging(signalR.LogLevel.Information)
        .build();

    connection.start().catch(e => console.log(e));

    connection.on("Send", (username, message, timestamp) => {
        AppendMessage(username, message, timestamp);
    });

    connection.onclose(async () => {
        await start();
    });

});

async function start() {
    try {
        await connection.start();
        console.log("SignalR Connected.");
    } catch (err) {
        console.log(err);
        setTimeout(start, 5000);
    }
};


function SendMessage() {
    let message = $("#NewMessage").val();

    if (!message) {
        return false;
    }

    let userName = $('.name').html();

    connection.invoke("Send", userName, message);

    $('#NewMessage').val('');
}

function AppendMessage(username, message, timestamp) {
    let len = $('#messages-container li').length;

    if (len >= 50) {
        for (let i = 50; i <= len; i++) {
            $(`#messages-container li:last-child`).remove();
        }
    }

    let toAppend = `<li><strong>[${timestamp}]</strong> <i>${username}</i>: ${message}</li>`;
    $('#messages-container').prepend(toAppend); 
}


