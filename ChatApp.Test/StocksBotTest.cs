﻿using ChatApp.StocksBot;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ChatApp.Test
{
    public class StocksBotTest
    {
        private readonly string baseUrl = "https://stooq.com/q/l/?s={%STOCKCODE%}&f=sd2t2ohlcv&h&e=csv";


        [Fact]
        public async void TestStockBot_ValidStockCode()
        {
            //Arrange
            string queueName = "testQueueValid";
            var factory = new ConnectionFactory() { HostName = "localhost" };

            var _stocksCheck = new StocksCheck(baseUrl, queueName, factory);
            string strMessage = "";
            bool findStockResult = false;

            //Act
            findStockResult = await _stocksCheck.FindStockPrice("aapl.us");

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: queueName,
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    var consumer = new EventingBasicConsumer(channel);

                    BasicGetResult result = channel.BasicGet(queue: queueName, autoAck: true);
                    strMessage = Encoding.UTF8.GetString(result.Body.ToArray());
                }
            }

            //Assert
            Assert.True(findStockResult);
            Assert.EndsWith("per share.", strMessage);

        }

        [Fact]
        public async void TestStockBot_InvalidStockCode()
        {
            //Arrange
            string queueName = "testQueueInvalid";
            var factory = new ConnectionFactory() { HostName = "localhost" };

            var _stocksCheck = new StocksCheck(baseUrl, queueName, factory);
            string strMessage = "";
            bool findStockResult = false;
            string invalidCode = "Invalid Stock Code";

            //Act
            findStockResult = await _stocksCheck.FindStockPrice(invalidCode);

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: queueName,
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    var consumer = new EventingBasicConsumer(channel);

                    BasicGetResult result = channel.BasicGet(queue: queueName, autoAck: true);
                    strMessage = Encoding.UTF8.GetString(result.Body.ToArray());
                }
            }

            //Assert
            Assert.True(findStockResult);
            Assert.Equal($"Value for stock code {invalidCode} could not be found", strMessage);
        }

    }
}
