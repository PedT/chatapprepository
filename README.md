**Overral description**

This is a simple chat application, where users can chat in a chatroom, and recover stock prices with the "/stock=stock_code" command.

You can register a new user by clicking on the "Register" option on the login screen.

---

## Database configuration

This application uses a postgreSQL database to store user information. 

If wish to use a differente database from the one that's configured: 

1. Change the connection string in the appsettings.json file
2. Open the package manager console, select the Data project
3. Run the "Update-Database" command.

---

## RabbitMQ

To use this application, you need RabbitMQ configured. The default configuration is a localhost instance, with no username or password defined.

If you wish to change this, paste your RabbitMQ settings on the "RabbitMQConnectionFactorySettings" section of the appsettings.json file.

The "RabbitMQConnectionFactorySettings" should reflect the data that you would configure on the ConnectionFactory RabbitMQ class.

