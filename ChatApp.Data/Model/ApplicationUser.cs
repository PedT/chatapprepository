﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp.Data.Model
{
    public class ApplicationUser : IdentityUser
    {
        [MaxLength(64)]
        public string DisplayName { get; set; }

    }
}
