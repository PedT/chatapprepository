﻿using ChatApp.Data.Model;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApp.Data.Context
{
    public class AppDbContext : IdentityDbContext<ApplicationUser>
    {
        public AppDbContext(DbContextOptions options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.UseSerialColumns();
            base.OnModelCreating(builder);

            foreach(var fk in builder.Model.GetEntityTypes().SelectMany( x=> x.GetForeignKeys())){
                fk.DeleteBehavior = DeleteBehavior.NoAction;
            }

        }

    }
}
